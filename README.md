# The Game of Dice

The "Game of Dice" is a multiplayer game where N players roll a 6 faced dice in a round-robin fashion. Each time a player rolls the dice their points increase by the number (1 to 6) achieved by the roll. As soon as a player accumulates M points they complete the game and are assigned a rank. Remaining players continue to play the game till they accumulate at least M points. The game ends when all players have accumulated at least M points.

## Rules of the game

- The order in which the users roll the dice is decided randomly at the start of the game.
- If a player rolls the value "6" then they immediately get another chance to roll again and move
ahead in the game.
- If a player rolls the value "1" two consecutive times then they are forced to skip their next turn
as a penalty.

*** 

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development, testing and evaluation purposes.

## Prerequisites


1. .NET Framework [4.6](https://dotnet.microsoft.com/en-us/download/dotnet-framework/net46) (supported by default in Windows 10 and above)
2. Visual Studio Code or any editor.
3. Programming Language - C#
4. Windows OS

## Running the program

- There is a precompiled .exe application. Run the application to play !
- Any modifications to the program can be compiled using command - csc <fileName.cs> from Windows command prompt. Please make sure to alter the “Path” variable under System variables so that it also contains the path to the .NET Framework environment


