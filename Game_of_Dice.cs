using System;
using System.Collections.Generic;
using System.Linq;

namespace GameOfDiceApp {

	public class GameOfDice {

		public class Player {
			public int id {get; set;}
			public string name {get; set;}
			public int previousValue {get; set;}
			public int score {get; set;}
			public bool shouldSkip {get; set;}
		}

		public static Random random = new Random();
		public static List<int> diceValues = new List<int>(){1,2,3,4,5,6};
		public static HashSet<int> diceValuesToRepeatChance = new HashSet<int>() { 6 };
		public static HashSet<int> diceValuesToSkipChance = new HashSet<int>() { 1 };

		public static void TheGameOfDice(int numberOfPlayers, int pointsToAccumulate) {
			Dictionary<int, Player> playerSet = InitializePlayerSet(numberOfPlayers);
			List<int> playerIds = playerSet.Keys.ToList();
			List<int> playersWithChanceToRoll = new List<int>(playerIds);
			int currentPlayerId;
			List<Player> fixedRankBoard = new List<Player> ();
			Dictionary<int, int> dynamicRankBoard = new Dictionary<int, int>();
			int rank = 0;
			//Assigns fixed order
			playerIds = AssignRandomOrderToPlayers(playerIds);
			int order = 0;
			while (true) {
				if (playersWithChanceToRoll.Count == 0) {
					Console.WriteLine("--------------------------------------------------------------------------------------");
					if (playerIds.Count == 0) {
						Console.WriteLine(string.Format("\nCongratulations! All players have accumulated {0} points. \n\nFinal rankings are - \n", pointsToAccumulate));
						PrintRankBoard(fixedRankBoard, dynamicRankBoard, playerSet);
						break;
					}
					playersWithChanceToRoll = new List<int>(playerIds);
					order = 0;
				}
				//Assigns random order each round
				//currentPlayerId = GetRandomIntValue(playersWithChanceToRoll);
				currentPlayerId = playerIds[order];
				Player currentPlayer = playerSet[currentPlayerId];
				Player newPlayerEntity = CapturePlayerInput(currentPlayer);
				playersWithChanceToRoll.Remove(currentPlayerId);
				bool hasDynamicRank = dynamicRankBoard.ContainsKey(currentPlayerId);
				if (newPlayerEntity.score >= pointsToAccumulate) {
					fixedRankBoard.Add(newPlayerEntity);
					Console.WriteLine(string.Format("{0} has completed the game by accumulating {1} points and gains rank {2} !\n", newPlayerEntity.name, newPlayerEntity.score, ++rank));
					if (hasDynamicRank) {
						dynamicRankBoard.Remove(currentPlayerId);
					}
					playerIds.Remove(currentPlayerId);
				}
				else {
					dynamicRankBoard[newPlayerEntity.id] = newPlayerEntity.score;
					dynamicRankBoard = dynamicRankBoard.OrderByDescending(d => d.Value).ToDictionary(d => d.Key, d => d.Value);
					order++;
				}
				Console.WriteLine("Updated Rank Board"); 
				PrintRankBoard(fixedRankBoard, dynamicRankBoard, playerSet);
				SetTimeOut(500);
			}
		}

		public static Dictionary<int, Player> InitializePlayerSet(int numberOfPlayers) {
			Dictionary<int, Player> playerSet = new Dictionary<int, Player>();
			for(int i = 1 ; i <= numberOfPlayers; i ++) {
				Player player = new Player() {
					id = i,
					name = string.Format("Player - {0}", i),
					score = 0,
					previousValue = 0,
					shouldSkip = false,
				};
				playerSet.Add(i, player);
			}	
			return playerSet;
		}

		public static List<int> AssignRandomOrderToPlayers(List<int> playerIdList) {
			int count = playerIdList.Count;
			List<int> orderPlayerIds = new List<int>();
			for (int i = 0; i < count ; i++) {
				var index = random.Next(playerIdList.Count);
				var value = playerIdList[index];
				orderPlayerIds.Add(value);
				playerIdList.Remove(value);
			}
			return orderPlayerIds;
		}

		public static Player CapturePlayerInput(Player player) {
			if(player.shouldSkip) {
				Console.WriteLine("Skipping {0} as penalty for consecutive {1} !", player.name, player.previousValue);
				player.shouldSkip = false;
				player.previousValue = 0;
			}
			else {
				bool retry = false;
				bool repeat = false;
				string playerName = player.name;
				string input;
				int currentValue = 0;
				int randomValue = 0;
				do {
					if(repeat) {
						SetTimeOut(500);
					}
					input = readInput(string.Format("{0} please press enter to roll the dice {1}!", playerName, repeat ? "again" : ""));
					retry = false;
					if (!string.IsNullOrEmpty(input)) {
						Console.WriteLine(string.Format("{0} please enter correct input", playerName));
						retry = true;
					}
					else {
						randomValue = GetRandomIntValue(diceValues);
						Console.WriteLine(string.Format("Dice roll output for {0} is : {1}\n", playerName, randomValue));
						if (diceValuesToRepeatChance.Contains(randomValue)) {
							retry = true;
							repeat = true;
						}
						else if (diceValuesToSkipChance.Contains(randomValue) && randomValue == player.previousValue) {
							player.shouldSkip = true;
						}
						player.previousValue = randomValue;
						currentValue = currentValue + randomValue;
					}
				} while (retry != false);
				player.score = player.score + currentValue;
			}
			return player;
		}

		public static void PrintRankBoard(List<Player> fixedRankBoard, Dictionary<int,int> dynamicRankBoard, Dictionary<int, Player> playerSet) {
			Console.WriteLine(" Rank  \t  PlayerName  \t  Score  \t");
			string tableOutputFormat = "  {0}  \t  {1}  \t  {2} \t";
			if(fixedRankBoard.Count > 0) {
				for(int i = 0; i < fixedRankBoard.Count; i ++) {
					Player player = fixedRankBoard[i];
					Console.WriteLine(string.Format(tableOutputFormat, i+1, player.name, player.score)); 
				}
			}
			int offSet = fixedRankBoard.Count + 1;
			if(dynamicRankBoard.Count > 0) {
				foreach(var keyValuePair in dynamicRankBoard) {
					Player player = playerSet[keyValuePair.Key];
					Console.WriteLine(string.Format(tableOutputFormat, offSet, player.name, player.score)); 
					offSet++;
				}
			}
			Console.WriteLine();
		}

		public static int GetRandomIntValue(List<int> valueList) {
			int index = random.Next(valueList.Count);
			int value = valueList[index];
			return value;
		}

		public static void SetTimeOut(int timeOutInMilliSeconds) {
			System.Threading.Thread.Sleep(timeOutInMilliSeconds);
		}

		public static string readInput(string prefix) {
			Console.WriteLine(prefix);
			string input = Console.ReadLine();
			return input;
		}

		static void Main() {
			Console.WriteLine("\nWelcome ! \n\n");
			var input = readInput("Enter the number of players in the game: ");
			int numberOfPlayers;
			if(!Int32.TryParse(input, out numberOfPlayers)) {
				throw new Exception("Please enter a valid number");
			}
			Console.WriteLine(string.Format("Number of players: {0}", numberOfPlayers));
			input = readInput("Enter the points to be accumulated by a player: ");
			int pointsToAccumulate;
			if (!Int32.TryParse(input, out pointsToAccumulate)) {
				throw new Exception("Please enter a valid number");
			}
			Console.WriteLine(string.Format("Points to be accumulated by a player: {0} \n", pointsToAccumulate));
			TheGameOfDice(numberOfPlayers, pointsToAccumulate);
			input = readInput("Please press any key to exit");
		}
	}
}